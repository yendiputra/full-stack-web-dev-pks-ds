//soal 1
//buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

var persegipanjang = (panjang, lebar) => panjang * lebar;
console.log(persegipanjang(2, 3));  // 6


// function KelilingPersegiPanjang() {
//     let panjang = 4;
//     let lebar = 5;
//     let keliling= 2*p*l
//   }
  
//   console.log(keliling)
//soal 2


const newFunction = (firstName, lastName)=>{
  return {
    firstName,
    lastName,
    fullName: `${firstName} ${lastName}`
  }
}
 
//Driver Code 
console.log(newFunction("William", "Imoh").fullName)

// soal 3

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
    ({ firstName, lastName, address, hobby } = newObject);

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;

// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
// Driver code

console.log(firstName, lastName, address, hobby)



// Soal 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// soal 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth" 
const view = "glass" 

const hasil= (`Lorem  ${view} dolor sit amet consectetur adipiscing elit
not ${planet}.`);
console.log(hasil)