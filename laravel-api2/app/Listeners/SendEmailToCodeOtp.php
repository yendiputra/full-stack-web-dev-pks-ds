<?php

namespace App\Listeners;

use App\Events\CommentStoreEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToCodeOtp
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentStoreEvent  $event
     * @return void
     */
    public function handle(CommentStoreEvent $event)
    {
        Mail::to($event->$otp->post->user->email)->send(new OtpAuthorMail($event->$otp));
    }
}
