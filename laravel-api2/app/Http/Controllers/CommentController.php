<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoreEvent;
use App\Mail\PostAuthorMail;
use App\Mail\CommentAuthorMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table comments
        $comments = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data comment',
            'data'    => $comments  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find comment by ID
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data comment',
            'data'    => $comment 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }



        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id
        ]);

        //memanggil ven commnetstoreEvent
        event(new CommentStoreEvent($comment));

        // //ini dikirim ke pada pemilik post
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        // //ini dikirim ke pada pemilik comment
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        //success save to database
        if($comment) {

            $user = auth()->user();
            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data ini bukan milik user yang login',
                    'data'    => $comment  
                ], 403);
            }

            return response()->json([
                'success' => true,
                'message' => 'comment Created',
                'data'    => $comment  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'comment Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $comment
     * @return void
     */
    public function update(Request $request, Comment $comment)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comment by ID
        $comment = Comment::findOrFail($comment->id);

        if($comment) {
            $user = auth()->user();
                if($comment->user_id != $user->id)
                {
                    return response()->json([
                        'success' => false,
                        'message' => 'Data ini bukan milik user yang login',
                        'data'    => $comment  
                    ], 403);
                }


            //update comment
            $comment->update([
                'content'     => $request->content,
                'post_id'   => $request->post_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'comment Updated',
                'data'    => $comment  
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'comment Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find comment by ID
        $comment = Comment::findOrfail($id);

        if($comment) {

            $user = auth()->user();
            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data ini bukan milik user yang login',
                    'data'    => $comment  
                ], 403);
            }

            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'comment Deleted',
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'comment Not Found',
        ], 404);
    }
}
