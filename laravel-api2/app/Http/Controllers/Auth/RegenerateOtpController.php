<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
    //     protected function validator(array $data)
    // {
        $allRequest = $request->all();
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $user =  User::where('email, $request')->first();

        do {
            $random = mt_rand(100000 , 9999999);
            $check = OtpCode::where('otp',$random)->first();
            # code...
        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp'=>$random,
            'valid_until'=>$now->addMiuutes(5)
        ]);
            return response()->json([
                'succes'=>true,
                'message'=>'Data user berhasil dibuat',
                'data'=>[
                    'user'=>$user,
                    'otp_code'=> $otp_code
                ]
                ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    }
}
