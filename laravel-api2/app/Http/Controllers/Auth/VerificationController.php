<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        if(!stop_code){
            
            return response()->json([
                'succes'=>false,
                'message'=>'otp code tidak ditemukan', 400]);
        }

    $now = Carbon::now();
    if ($now ->$otp_code->valid_until) {
        return response()->json([
            'succes'=>false,
            'message'=>'otp code tidak berlaku lagi']);
    }
        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at'=> $now
        ]);

        $otp_code->delete();
        return response()->json([
            'succes'=>true,
            'message'=>'user berhasil di verifikasi']);

    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    }
    }
}
