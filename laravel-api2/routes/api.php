<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('/post', 'PostController');
Route::apiResource('/comment', 'CommentController');
Route::apiResource('/role', 'RoleController');

// Route::grup([
//     'prefix'=>'auth',
//     'namespace'=>'Auth'
// ], function(){
//     Route::post('auth/register', 'Auth\RegisterController')->name('auth.register');

//     Route::post('regenerate-otp-code', 'RegenerateOtpController')->name('auth.regenerate-otp-code');

//     Route::post('verification', 'VerifikationController')->name('auth.verification');
// });