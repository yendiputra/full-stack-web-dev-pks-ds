@extends('layout.master')
@section('judul')
Halaman Registrasi
@endsection
@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up</h3> 
    <Form action="welcome" method="GET">
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br>
        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname"><br>

        <p>Gender:</p>
        <input type="radio" id="male" name="male" value="30">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="female" value="60">
        <label for="female">Female</label><br>
        <input type="radio" id="ohter" name="ohter" value="100">
        <label for="ohter">Other</label><br><br>

        <p>Nationality:</p>
        <select name="Nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>    
            <option value="other">Other</option>
        </select>

        <p>Bio</p>
        <textarea id="bio" name="bio" rows="5" cols="20"> </textarea>
        <br>
        
        <input type="submit" value="Sing Up">
    </form>
@endsection