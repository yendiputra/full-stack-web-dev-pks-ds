@extends('layout.master')
@section('judul')
Halaman Detail Game {{ $game->nama }}
@endsection
@section('content')

<h3>Nama : {{ $game->nama }}</h3>
<p>Gameplay :{{ $game->gameplay }} </p>
<p>Developer :{{ $game->developer }}</p>
<p>Year :{{ $game->year }}</p>

@endsection