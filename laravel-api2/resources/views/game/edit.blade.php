@extends('layout.master')
@section('judul')
Edit Data game {{ $game->nama }}
@endsection
@section('content')

<form action="/game/{{ $game->id }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="exampleFormControlInput1">Nama</label>
      <input type="text" name="nama" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value={{ $game->nama }}>
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
          <label for="exampleFormControlTextarea1">Gameplay</label>
          <textarea name="gameplay" class="form-control" id="exampleFormControlTextarea1" rows="3" >{{ $game->gameplay }}</textarea>
        </div>
        @error('gameplay')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label for="exampleFormControlInput1">Developer</label>
        <input type="text" name="developer" class="form-control" id="exampleFormControlInput1" placeholder="developer" value={{ $game->developer }}>
      </div>
      @error('developer')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label for="exampleFormControlInput1">Year</label>
      <input type="text" name="year" class="form-control" id="exampleFormControlInput1" placeholder="year" value={{ $game->year }}>
    </div>
    @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-warning">Update</button>
  </form>

@endsection