@extends('layout.master')
@section('judul')
Tambah Data Game
@endsection
@section('content')

<form action="/game" method="post">
    @csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Nama</label>
      <input type="text" name="nama" class="form-control" id="exampleFormControlInput1" placeholder="Nama Game">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label for="exampleFormControlInput1">gameplay</label>
        <input type="text" name="gameplay" class="form-control" id="exampleFormControlInput1" placeholder="gameplay">
      </div>
      @error('gameplay')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">developer</label>
      <textarea name="developer" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    @error('developer')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleFormControlInput1">year</label>
      <input type="text" name="year" class="form-control" id="exampleFormControlInput1" placeholder="year">
    </div>
    @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>

@endsection