@extends('layout.master')
@section('judul')
List Data Game
@endsection
@section('content')
<a href="/game/create" class="btn btn-success">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Gameplay</th>
                <th scope="col">Developer</th>
                <th scope="col">Year</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($game as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->gameplay}}</td>
                        <td>{{$value->developer}}</td>
                        <td>{{$value->year}}</td>
                        <td>
                            
                            <form action="/game/{{$value->id}}" method="POST">
                                <a href="/game/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/game/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Data  tidak ada</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection