@extends('layout.master')
@section('judul')
Edit Data Cast {{ $cast->nama }}
@endsection
@section('content')

<form action="/cast/{{ $cast->id }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="exampleFormControlInput1">Nama</label>
      <input type="text" name="nama" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value={{ $cast->nama }}>
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label for="exampleFormControlInput1">Umur</label>
        <input type="text" name="umur" class="form-control" id="exampleFormControlInput1" placeholder="Umur" value={{ $cast->umur }}>
      </div>
      @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Bio</label>
      <textarea name="bio" class="form-control" id="exampleFormControlTextarea1" rows="3" >{{ $cast->bio }}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-warning">Update</button>
  </form>

@endsection