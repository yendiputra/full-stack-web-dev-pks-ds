@extends('layout.master')
@section('judul')
Tambah Data Cast
@endsection
@section('content')

<form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Nama</label>
      <input type="text" name="nama" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label for="exampleFormControlInput1">Umur</label>
        <input type="text" name="umur" class="form-control" id="exampleFormControlInput1" placeholder="Umur">
      </div>
      @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Bio</label>
      <textarea name="bio" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>

@endsection