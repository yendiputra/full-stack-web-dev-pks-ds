// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
    daftarHewan.sort();
    daftarHewan.forEach(function(item){
   console.log(item)
})

// soal 2
 function introduce() {
  return data
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// soal 3

function hitung_huruf_vokal(str) { 

    // cari huruf vokal aiueo
    const jumlah = str.match(/[aeiou]/gi).length;

    // kembalikan nilai dari huruf fokal
    return jumlah;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

//Soal 4
//Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.
function hitung(angka) {
    return  angka *2 - 2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log(hitung(5)) // 8
console.log(hitung(6)) // 10


